<?php
    require 'ics-parser/class.iCalReader.php';
    $ical = new ICal("https://www.google.com/calendar/ical/swansea-union.co.uk_efaaset0r2t6qd7omcmkpqvbd0%40group.calendar.google.com/public/basic.ics");
    $events = $ical->events();

    $society = $_GET["society"];
    $first = true;

    header('Content-Type: application/json');
    echo "[";

    foreach($events as $event) {
        if (!$society || strpos($event['SUMMARY'],$society) !== false){
            if(!$first){
                echo ",";
            } else {
                $first = 0;
            }
            echo json_encode([
                summary => $event['SUMMARY'],
                time => date_parse($event["DTSTART"])
            ]);
        }
    }
    echo "]";
?>
